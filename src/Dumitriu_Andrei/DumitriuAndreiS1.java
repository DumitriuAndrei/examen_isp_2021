package Dumitriu_Andrei;

import java.util.ArrayList;
import java.util.List;

public class DumitriuAndreiS1 {
}

class Phone {

    Battery battery = new Battery();
    Display display = new Display();
    GPSModule gpsModule;
    List<Button> button;

    public Phone(GPSModule gpsModule, List<Button> button) {
        this.gpsModule = gpsModule;
        this.button = button;
    }

    public void batteryMethod(Battery battery){}
    public void displayMethod(Display display){}
}

class GPSModule {

}

class Display {
    List<Button> buttons = new ArrayList<>();
}

class Battery {

}

class Button {

}

class User {
    boolean hasPhone(Phone phone) {
        return phone == null;
    }
}
