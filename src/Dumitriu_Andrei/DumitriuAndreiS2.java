package Dumitriu_Andrei;

public class DumitriuAndreiS2 {
}
class NiceThread extends Thread {
    private String name;

    NiceThread(String name) {
        super(name);
    }

    public void run() {
        for (int i = 0; i < 9; i++) {
            System.out.println(getName() + " - " + i);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(getName() + " is done.");
    }

}
class MainClass{
    public static void main(String[] args) {

        NiceThread NiceThread1 = new NiceThread("NiceThread1");
        NiceThread NiceThread2 = new NiceThread("NiceThread2");
        NiceThread NiceThread3 = new NiceThread("NiceThread3");

        NiceThread1.start();
        NiceThread2.start();
        NiceThread3.start();
    }
}

